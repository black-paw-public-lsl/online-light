# Online Light

This is a script to turns a light on and off to indicate whether a user is logged in.

## Use

In order to control the light,
the user's UUID is put into the object's description.

This script takes the UUID in the object description,
and determines whether they're logged in.

Help text is shown if the script cannot find a valid UUID in the object description.

## Customization

### Defines

This script was created with the assumption
the Firestorm script extensions are available,
specifically the ability to use the `#define`, `#ifdef` and  `#endif` keywords.

The author checks code out using git,
and compiles scripts directly off of her drive.

For example:

```lsl
#define ON_COLOR <0, 0, 1>
#define OFF_INTENSITY 0
#define PRIM_FACE 2
#include "black-paw-public-lsl/online-light.lsl"
```

**This is not a requirement,**
but it influences the way the script is intended to be customized.

If you do not use this method,
you can simply put your own `#define` statements at the top of the script.

### Light Settings

The light settings for on and off are controlled the the following defines:

* `ON_COLOR`      - A vector for the color     of the light when it's on.
* `OFF_COLOR`     - A vector for the color     of the light when it's off.
* `ON_GLOW`       - A number for the glow      of the light when it's on.
* `OFF_GLOW`      - A number for the glow      of the light when it's on.
* `ON_INTENSITY`  - A number for the intensity of the light when it's on.
* `OFF_INTENSITY` - A number for the intensity of the light when it's off.
* `ON_RADIUS`     - A number for the radius    of the light when it's on.
* `OFF_RADIUS`    - A number for the radius    of the light when it's off.
* `ON_FALLOFF`    - A number for the falloff   of the light when it's on.
* `OFF_FALLOFF`   - A number for the falloff   of the light when it's off.

The defaults are for a warm light that's bright or dim.

For example, to use a blue light when the user is logged in and completely dark when they are not,

```lsl
#define ON_COLOR <0, 0, 1>
#define OFF_INTENSITY 0
```

### How Often To Check

How often the script checks for online status is controlled by two defines.

The script will pick a randon number between the two values.
This is to avoid multiple status boards from firing at the same time.

* `ONLINE_STATUS_CHECK_INTERVAL_LOW`  - The minimum number of seconds between checks. Default is 60 seconds.
* `ONLINE_STATUS_CHECK_INTERVAL_HIGH` - The maximum number of seconds between checks. Default is 120 seconds.

### Prim Face

The script will optionally set prim faces to the same color as the light.
By default it sets all faces to the light color.

To control the faces number it changes,
define the `PRIM_FACE` define.

For example, to change the color of face 3:

```lsl
#define PRIM_FACE 3
```

To disable coloring prim faces by the script,
define the `NO_PRIM_FACE` define.

For example, to disable prim face coloring:

```lsl
#define NO_PRIM_FACE
```

### Hover Text Color

Help text color is controlled by defining `HOVER_TEXT_COLOR`.
If this is not defined,
it defaults to yellow.

For example, to display red help text:

```lsl
#define HOVER_TEXT_COLOR <1, 0, 0>
```

### Debugging Information

If `DEBUG` is not defined,
the script will not compile in the `debug()` statements.

If `DEBUG` is defined,
the script expects a fuction with this signature to exist:

```lsl
debug(string message)
{
    ...
}
```

```lsl
#define DEBUG
debug(string message)
{
    llOwnerSay(message);
}
```

or

```lsl
#include "lib/debug.lsl"
```

if you use such a thing.
