// If you put this code directly into a script rather than using
// #include, the put your customizations here.
// Read the included README for details.



//------------------------------------------------------------------------------
// Do not change code below.
//------------------------------------------------------------------------------

// Comment out the next line if you don't have a tool file like this.
// #include "lib/debug.lsl"
#ifndef DEBUG
#define debug(a)
#endif

// Hover text while initializing.
#ifndef HOVER_TEXT_COLOR
#define HOVER_TEXT_COLOR <1.0, 1.0, 0.0>
#endif

// Face(s) to color
#ifndef PRIM_FACE
#define PRIM_FACE ALL_SIDES
#endif

#ifndef ON_COLOR
#define ON_COLOR <1.0, 0.8, 0.4>
#endif

#ifndef OFF_COLOR
#define OFF_COLOR <1,1,1>
#endif

#ifndef ON_GLOW
#define ON_GLOW 0.2
#endif

#ifndef OFF_GLOW
#define OFF_GLOW 0.0
#endif

#ifndef ON_INTENSITY
#define ON_INTENSITY 1.0
#endif

#ifndef OFF_INTENSITY
#define OFF_INTENSITY ON_INTENSITY
#endif

#ifndef ON_RADIUS
#define ON_RADIUS 10
#endif

#ifndef OFF_RADIUS
#define OFF_RADIUS ON_RADIUS
#endif

#ifndef ON_FALLOFF
#define ON_FALLOFF 0.0
#endif

#ifndef OFF_FALLOFF
#define OFF_FALLOFF ON_FALLOFF
#endif

#ifndef LIGHT_ON
#define LIGHT_ON [ \
        PRIM_POINT_LIGHT, TRUE, ON_COLOR, ON_INTENSITY, ON_RADIUS, ON_FALLOFF, \
        PRIM_FULLBRIGHT, ALL_SIDES, TRUE, \
        PRIM_GLOW, ALL_SIDES, ON_GLOW]
#endif

#ifndef LIGHT_OFF
#define LIGHT_OFF [ \
        PRIM_POINT_LIGHT, FALSE, OFF_COLOR, OFF_INTENSITY, OFF_RADIUS, OFF_FALLOFF, \
        PRIM_FULLBRIGHT, ALL_SIDES, TRUE, \
        PRIM_GLOW, ALL_SIDES, OFF_GLOW]
#endif

//------------------------------------------------------------------------------

// How often should the board update the user's status?
#ifndef ONLINE_STATUS_CHECK_INTERVAL_LOW
#define ONLINE_STATUS_CHECK_INTERVAL_LOW 60.0
#endif

#ifndef ONLINE_STATUS_CHECK_INTERVAL_HIGH
#define ONLINE_STATUS_CHECK_INTERVAL_HIGH 120.0
#endif

request_online_status(key agent)
{
    // There's not much sense in recording the request key as
    // we're making simple requests that will simply not fire if
    // they fail.
    if(agent)
        llRequestAgentData(agent, DATA_ONLINE);
}

// Add a random factor so multiple boards aren't all firing at the same time.
set_update_interval()
{
    float interval = ONLINE_STATUS_CHECK_INTERVAL_LOW + llFrand(
        ONLINE_STATUS_CHECK_INTERVAL_HIGH - ONLINE_STATUS_CHECK_INTERVAL_LOW
    );

    llSetTimerEvent(interval);
}

//------------------------------------------------------------------------------

// Look in the description for a UUID.
// If there is not one, use llGetOwner().
// This will allow a manager to set up multiple boards for staff.
string last_object_description;
key agent_key;

// I cannot see any automatic LSL trigger for changed descriptions.
// This substitutes for one.
string error_text = "";
check_description(integer force)
{
    // See if the description has changed.
    string description = llGetObjectDesc();
    if(!force && description == last_object_description) return;

    debug("Reading UUID from description.");
    last_object_description = description;
    error_text = "";

    // Split the description into pieces:
    // Agent UUID, Online Image UUID, Offline Image UUID
    list parts = llParseString2List(description, ["|", ","], []);

    // Get the agent's key.
    key new_key = llList2Key(parts, 0);
    if(new_key)
    {
        debug("Found a valid UUID (" + string(new_key) + "). Assuming it is an avatar's.");
        agent_key = new_key;
    }
    else
    {
        debug("No valid UUID was found. Using object owner.");
        error_text = error_text + "Invalid person UUID.\n";
        agent_key = NULL_KEY;
    }

    if(error_text != "")
        error_text = error_text + "Ensure there are no spaces before or after UUID.\n";

    llSetText(error_text, HOVER_TEXT_COLOR, 1);
}

init()
{
    llSetText("Initializing...", HOVER_TEXT_COLOR, 1);
    set_update_interval();
    // Force reading from the description.
    check_description(TRUE);
    request_online_status(agent_key);
}

default
{
    on_rez(integer param)
    {
        llResetScript();
    }

    state_entry()
    {
        init();
    }

    timer()
    {
        check_description(FALSE);
        request_online_status(agent_key);
    }

    touch_start(integer touch_num)
    {
        // Touch means force an update.
        check_description(TRUE);
        request_online_status(agent_key);
    }

    dataserver(key request, string data)
    {
        if (data == "1")
        {
            #ifndef NO_PRIM_FACE
            llSetColor(ON_COLOR, PRIM_FACE);
            #endif
            llSetPrimitiveParams(LIGHT_ON);
        }
        else
        {
            #ifndef NO_PRIM_FACE
            llSetColor(OFF_COLOR, PRIM_FACE);
            #endif
            llSetPrimitiveParams(LIGHT_OFF);
        }
    }
}
